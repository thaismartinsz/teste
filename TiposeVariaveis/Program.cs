﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiposeVariaveis
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero1 = 10; // declara uma variavel inteira e armazenao valor 10 nela
            int numero2 = 20;
            var soma = numero1 + numero2; //o var define dinamicamente o tipo de varavel
            Console.WriteLine("10 + 20 = " + soma);

            int copiadenumero1 = numero1; //copia o valor da variavel "numero1"
            copiadenumero1 = 11; // será que o valor de "numero1" foi alterado?
            Console.WriteLine(numero1);
            Console.WriteLine(copiadenumero1);

            var quadrado1 = new Quadrado(); // cria um quadrado
            quadrado1.lado = 10; //quadrado1 terá o valor de lado = 10
            var quadrado2 = quadrado1; // será que a cópia de quadrado foi completa? Ou copiamos apenas a referencia?
            quadrado2.lado = 11;
            Console.WriteLine(quadrado1.lado); // quadrado1 deveria ter o lado igual a 10
            Console.WriteLine(quadrado2.lado); // quadrado2 deveria ter o lado igual a 11

            Console.ReadKey();
        }

        class Quadrado // defina uma classe chamada Quadrado
        {
            public int lado; // Define um atributo inteiro chamado lado na classe quadrado
        }
    }
}




