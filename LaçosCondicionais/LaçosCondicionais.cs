﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaçosCondicionais
{
    class LaçosCondicionais
    {
        static void Main(string[] args)
        {
            //// Laços condicionais => execução mediante determinada condição/situação satisfeita
            //int hora = 18;

            //// laço if, else if e else

            //if (hora <= 15) // se a hora for maior ou igual a 15, será impressa a mensagem no console
            //    Console.WriteLine("É menos que 15h00!");
            //else if (hora <= 17)
            //    Console.WriteLine("A hora é maior que 17!");
            //if (hora <= 17)
            //    Console.WriteLine(" A hora é menor ou igual a 17!");
            //else // o else garante a execução do bloco de código seguinte a ele
            //{
            //    if (hora == 18)
            //        Console.WriteLine("São 18h!");
            //    else
            //        Console.WriteLine("Já passou das 18h");

            //}

            // switch.. case
            // geramente a expressão avaliada é uma constante
            switch (hora)
            {
                case 15: // o teste condicional aqui é exato
                    Console.WriteLine("São 15h!");
                    break;
            }

            PrimeiroSemestre mes = PrimeiroSemestre.Maio;
            NewMethod(mes);

            Console.ReadKey();
        }

        private static void NewMethod(PrimeiroSemestre mes)
        {
            switch (mes)
            {
                    case PrimeiroSemestre.Janeiro:
                        Console.WriteLine("Estamos em Janeiro!");
                        break;
                    case PrimeiroSemestre.Fevereiro:
                        Console.WriteLine("Estamos em Fevereiro!");
                        break;
                    case PrimeiroSemestre.Marco:
                        Console.WriteLine("Estamos em Marco");
                        break;
                    case PrimeiroSemestre.Abril:
                        Console.WriteLine("Estamos em Abril!");
                    break;

                    Case PrimeiroSemestre.Maio:
                        Console.WriteLine("Estamos em Maio!");
                    break;
                    Case PrimeiroSemestre.Junho:
                        Console.WriteLine("Estamos em Junho");
                    break;

            }
        }
    }

    public enum PrimeiroSemestre
    {
        Janeiro, Fevereiro, Marco, Abril, Maio, Junho
    }
}
