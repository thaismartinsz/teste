﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// pasta lógica
namespace HelloWord
{
    class Program
    {
        // Isso é um comentário de uma linha! Tudo que for escrito será ignorado
        // pelo compilador

        /*Este
        é
        um
        comentário
        de
        apenas
        uma
        linha
        */

        //Método Main;
        //Ponto de entrada para a aplicação, ou seja, é 
        // o que define o inicio da aplicação

        static void Main(string[] args)
        {
            ///Imprimir na tela o famoso "Olá mundo";
            Console.WriteLine("Olá, mundo!");
            Console.WriteLine("Meu nome é Thais");
            Console.ReadKey();
        }
    }
}
